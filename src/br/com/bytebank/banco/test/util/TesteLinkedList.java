package br.com.bytebank.banco.test.util;

import java.util.LinkedList;

import br.com.bytebank.banco.modelo.Conta;
import br.com.bytebank.banco.modelo.ContaCorrente;

public class TesteLinkedList {
	public static void main(String[] args) {
		LinkedList<Conta> listaDeContas = new LinkedList<Conta>();

		Conta cc = new ContaCorrente(22, 11);

		listaDeContas.add(cc);

		Conta cc2 = new ContaCorrente(22, 22);
		listaDeContas.add(cc2);

		Conta cc3 = new ContaCorrente(22, 22);
		// listaDeContas.add(cc3);

		boolean existe = listaDeContas.contains(cc3); // usa o methodo equals

		System.out.println("Existe? " + existe);

		for (Conta conta : listaDeContas) {
			if (conta.equals(cc3)) {
				System.out.println("igual");
			}
		}

		for (Conta conta : listaDeContas) {
			System.out.println(conta);
		}
	}
}
