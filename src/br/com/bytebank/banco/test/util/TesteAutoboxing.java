package br.com.bytebank.banco.test.util;

import java.util.ArrayList;
import java.util.List;

public class TesteAutoboxing {
	public static void main(String[] args) {
		int[] idades = new int[5];
		String[] nomes = new String[5];

		int idadeA = 29; // 29 primitivo
		Integer idadeB = new Integer(29); // -> 29 referencia -- deprecated
		// Integer, Boolean, Double, sao Wrappers.
		// Criar a referencia usando o construtor esta deprecated. Usar o Type.valueOf()

		Integer idadeC = Integer.valueOf(29); // boxing
		int valor = idadeC.intValue(); // unboxing

		List<Integer> numeros = new ArrayList<Integer>(); // nao funciona <int>

		numeros.add(29); // -> faz o autoboxing do primitivo 29 pro ponteiro que aponta para 29

		String num = "9";
		int parsedNum = Integer.parseInt(num);

		int ref = Integer.valueOf("3");
		ref++;
		System.out.println(ref);

	}
}
